# ansible-repo


## License
MIT / BSD


## Donations
If you found some value and would like ot support this and future projects you can donate:

**BTC** 3LX9cdyXvbAdhUnCteKVtS7uxQz2mqeLfB  
**LTC** MHKgdcZxcyC937otEqwMWvSPANmRVXdbyC  
**ETH** 0xe2D5c108C8219f97d7a3a91558ff11138eEd2814  
**DGB** DJgNTXkyG5u6rujVMZFQDPtf7ueSgAomNw  
**BCH** qz4lm4fxn8tvrze33afr85sfrz73uz5vwcycyngphk
