# Role: node_exporter
Role install Prometheus Node Exporter.


## Requirements
Role creates dedicated user for Node Exporter and during that process requires additional commands, which might not be installed by default:
 - **pwgen**
 - **mkpasswd**

 **NOTE**: Required packages can be installed using role in [ansible-role-systools](https://gitlab.com/qf3l3k/ansible-role-systools) repository.


## Variables
 - **node_exporter_release_url** - location of Node Exporter binaries for Linux from [Prometheus website](https://prometheus.io/download/#node_exporter)
 - **node_exporter_release_checksum** - checksum of Node Exporter archive from [Prometheus website](https://prometheus.io/download/#node_exporter)
 - **node_exporter_release_download_path** - location where archive will be downloaded
 - **node_exporter_user** - user name for Node Exporter service to run
 - **node_exporter_group** - group name for Node Exporter service to run
 - **node_exporter_home** - home directory for Node Exporter files
 - **node_exporter_service_file** - location of systemd service file
 - **node_exporter_remote_user** - Node Exporter remote authentication (not in use at the moment)
 - **node_exporter_remote_pass** - Node Exporter remote authentication (not in use at the moment)


## Dependencies
Role was developed and tested on Ubuntu.

Tested on:
 - Ubuntu 18.04, 20.04


## Example playbook
```yaml
- hosts: server01
  gather_facts: True
  become: True
  roles:
    - { role: node_exporter }
```


## License
MIT


