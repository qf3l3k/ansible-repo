# Role: letsencrypt
Role is responsible for Let's Encrypt certificates renewal.


## Requirements
Role has no specific requirements.


## Variables

 - **letsencrypt_install_certbot** - defines, if role should install **certbot** package (works on Ubuntu only)
 - **lets_encrypt_copy_cert_files** - defines, if cert files have to be copied in non-standard places after renewal
 - **letsencrypt_universe_apt_url** - package repository required by Ubuntu to install **certbot** package
 - **letsencrypt_apt_url** - package repository required by Ubuntu to install **certbot** package
 - **letsencrypt_debian_packages_to_install** - defines packages to install on Ubuntu, when **letsencrypt_install_certbot** is set to ***true***
 - **letsencrypt_cert_renewal_list** - list of certificates for renewal
 - **letsencrypt_services_to_stop** - services which should be stopped during renewal. It is important as renewal process needs to setup temporary web service, so anything which collides with it needs tobe stopped.
 - **letsencrypt_cert_file_copy_list** - list of certificates and locations where certificates should be copied after renewal. Some services can't load certificates from default system locations.


## Dependencies
Role was developed and tested on Ubuntu and as of now it recognizes Ubuntu only.
It is important mainly as in case Ubuntu is detected role can install **certbot** package if needed.
Tested on:
 - Ubuntu 18.04, 20.04


## Example playbook
```yaml
- hosts: server01
  roles:
    - { role: letsencrypt }
```


## License
MIT


