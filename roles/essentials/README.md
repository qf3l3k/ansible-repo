# Role: essentials
Role configures fresh system to be manageable using Ansible. It also deploys Golang with defined version and makes it available system-wide.


## Operating system
Role was developed and tested on Ubuntu and as of now it recognizes Ubuntu only.


## Requirements
Role requires to provide credentials to remote system in Ansible inventory file.
As credentials will be passed during tasks execution ```sshpass``` is required on Ansible host.


## Variables

- **essentials_sudoers_folder** - folder where *sudo* configuration files are stored.
- **essentials_user_name** - name of the user, which will be created. It should be account used by Ansible host for remote management.
- **essentials_group_name** - name of the group created along with the user. Usually same as user name.
- **essentials_user_password** - user password in clear text.
- **essentials_user_home** - path user home directory.
- **essentials_user_bin** - path to user-specific binary files. Used to store binaries, which don't need to be available system-wide. Folder is automatically added to *${PATH}*
- **essentials_golang_version** - version fo Golang, which should be installed.
- **essentials_golang_url** - URL for Golang. It is built using version fo Golang, so it is important to define version correctly.
- **essentials_golang_package_name** - name of archive with Golang extracted from download URL.
- **essentials_golang_download_path** - temporary location for Golang to be downloaded.
- **essentials_golang_install_path** - location for Golang installation. By default, as recommended on Golang website, it will be installed in */usr/local/go*.
- **essentials_goland_owner** - name of the user which should be owner of Goland files. As these are system-wie binaries, root is set as the owner user.
- **essentials_goland_group** - name of the group which should be owner of Goland files. As these are system-wie binaries, root is set as the owner group.
- **essentials_ansible_ssh_pubkey** - Public key for SSH access. This shoudl be your Ansible SSH public key.



## Dependencies
Tested on:
 - Ubuntu 18.04, 20.04


## Examples

### Inventory
```ini
[unconfigured]
hostname.example.com ansible_user=EXISTING_USER ansible_password=PASSWORD
```
**NOTE:** As playbook is to configure remote host to use with Ansible in future, for first time configuration credentials have to be provided, so Ansible can connect to host and execute playbook. It can be *root* user. If you specify normal user make sure it has *sudo* permissions without password requirement. In order to allow Ansible to use host specific credentials **sshpass** needs to be installed on Ansible host prior to playbook execution.
<br />
### Playbook
```yaml
---
- hosts: unconfigured
  gather_facts: True
  become: True
  roles:
    - essentials
```
**NOTE:** Unconfigured is name of host group defined in inventory file example above.
<br />
### Changes in ansible.cfg
```ini
[defaults]
host_key_checking = false
```
**NOTE:** 


## License
MIT / BSD
 

