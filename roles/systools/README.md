# Role: systools
Role deploys set of defined system tools.


## Requirements
Role has no specific requirements.


## Dependencies
Playbook identifies Debian and RedHat based distribution and applies distro-specific tasks.
Tested on:
 - Ubuntu 18.04, 16.04
 - CentOS 8.1


## Variables
 - **epel_repo_key** - key to EPEL repository for RedHat/CentOS
 - **epel_repo_url** - URL to EPEL repository for RedHat/CentOS
 - **packages_list** - list of packages to deploy


## Example playbook
```yaml
- hosts: servers
  roles:
    - { role: systools }
```

## License
MIT


